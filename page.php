<?php 
  //this is the page.php template
  //called to render a single page content
  
  get_header(); 
?>
<section class="main-section">
	<div class="wrapper">
		<?php
			if( have_posts() ):
				while ( have_posts() ) : 
          the_post();
          
          echo '<h1>'.get_the_title().'</h1>';
          the_content();

				endwhile;
			endif;
		?>
	</div>
</section>
<?php get_footer(); ?>