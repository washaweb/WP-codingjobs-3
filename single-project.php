<?php 
  //this is the single-project.php template
  //called to render a single project content
  
  get_header(); 
?>
<section class="main-section">
	<div class="wrapper">
		<?php
			if( have_posts() ):
				while ( have_posts() ) : 
					the_post();
					?>
					<div class="project-content">
						<?php the_post_thumbnail( 'large' ); ?>
						<h1><?php the_title() ?></h1>
						<p>
							<!-- get_post_meta(<postID>, <name of the custom field>, <boolean true:to get only the value, false: both the name and value pair>) -->
							Client: <?= get_post_meta( get_the_ID(), 'client', true ) ?><br>
							Project dates: <br>
							<?php the_field('project_start_date'); ?> - 
							<?php the_field('project_end_date'); ?>
						</p>
					</div>
					<?php
          the_content();

				endwhile;
			endif;
		?>
	</div>
</section>
<?php get_footer(); ?>