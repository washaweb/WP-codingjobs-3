<?php 
  //this is a specific page-contact.php template for the page with a slug like 'contact'
  //called to render the contact page content
  get_header(); 
?>
<section class="main-section">
	<div class="wrapper">
		<?php
			if( have_posts() ):
				while ( have_posts() ) : 
          the_post();
          
          echo '<h1>'.get_the_title().'</h1>';
					
					the_content();

				endwhile;
			endif;


			//my program comes here
			echo 'My specific template for the contact page';
			//ends here
			
		?>
	</div>
</section>
<?php get_footer(); ?>