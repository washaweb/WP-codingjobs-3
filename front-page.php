<?php get_header(); ?>

		<section class="jumbotron">
			<div class="wrapper">
				<h2>We are digital &amp; branding agency based in London.</h2>
				<h3>We love to turn great ideas into beautiful products.</h3>
				<a href="#" class="button">See portfolio</a>
			</div>
		</section>
		<!-- ./jumbotron -->

		<section id="section-icons" class="wrapper">
			<div class="container">
				<div class="col">
					<i class="icon lamp"></i>
					<h4>Pellentesque</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, quasi facere, animi maxime natus cupiditate</p>
				</div>
				<!-- ./col1 -->
				<div class="col">
					<i class="icon clock"></i>
					<h4>Consectetur</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, quasi facere, animi maxime natus cupiditate</p>
				</div>
				<!-- ./col2 -->
				<div class="col">
					<i class="icon flask"></i>
					<h4>Tristiquet</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, quasi facere, animi maxime natus cupiditate</p>
				</div>
				<!-- ./col3 -->
				<div class="col">
					<i class="icon ticket"></i>
					<h4>Fermentum</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, quasi facere, animi maxime natus cupiditate</p>
				</div>
				<!-- ./col4 -->
			</div>

			<hr />

		</section>
		
		<section id="section-latest-work" class="wrapper">
			<h3>Our latest works</h3>
			<div class="container">
				<article class="col">
					<img src="<?= get_template_directory_uri() ?>/img/image1.jpg" alt="Business Card">
					<h4>Nobis Business Card</h4>
					<h5>Business Cards, Graphics</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</article>

				<article class="col">
					<img src="<?= get_template_directory_uri() ?>/img/image2.jpg" alt="New fun project">
					<h4>New fun project</h4>
					<h5>Webdesign, Application</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</article>
				
				<article class="col">
					<img src="<?= get_template_directory_uri() ?>/img/image3.jpg" alt="Passionaries Branding Cover">
					<h4>Passionaries Branding Cover</h4>
					<h5>Branding, Graphic Design</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</article>
			</div>
		</section>
		
		<section id="section-latest-posts" class="wrapper">
			<h3>Our latest posts</h3>
			<div class="container">
				<?php 
					//define arguments for the custom query
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3,
						// 'category_name' => 'graphic-design'
					);
					
					//requesting data in a new Query call, using our arguments
					$query = new WP_Query($args);
				
					if( $query->have_posts( ) ):
						while( $query->have_posts() ): 
						$query->the_post(); 
					?>

						<article class="col">
							<?php if( has_post_thumbnail() ): 
								the_post_thumbnail( 'home-thumb' );
							else: ?>
								<img src="<?= get_template_directory_uri() ?>/img/image1.jpg" alt="Business Card">
							<?php endif; ?>
							<h4><?php the_title(); ?></h4>
							<h5><?php the_category(', '); ?></h5>
							<p><?php the_excerpt(); ?></p>
						</article>
					<?php endwhile;
				endif;
				//reset the post datas after the custom query
				wp_reset_postdata();
				
				?>
			</div>
		</section>
<?php get_footer(); ?>