  </main>
	<!-- ./main -->

<?php if( is_front_page() ): ?>
	<section id="section-testimony">
		<blockquote class="wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque voluptatum, quibusdam temporibus voluptas repudiandae hic maiores eligendi repellendus, accusamus nobis laboriosam</p>
		</blockquote>
	</section>
	<!-- ./testimony -->
<?php endif; ?>


	<footer class="main-footer">
		<div class="wrapper">
			<div class="container">

				<?php dynamic_sidebar( 'footer_sidebar' ) ?>

				<!-- ./col1 -->
<!-- 
				<div class="col">
					<h4>Twitter</h4>
					<ul class="twitter-list">
						<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam autem assumend
						</li>
						<li>
							Vitae ullam ipsam rem ratione sit facere ut. Natus voluptates fugiat quaerat
						</li>
						<li>
							Sapiente perferendis quis consequatur exercitationem sed facilis.
						</li>
					</ul>
				</div>

				<div class="col">
					<h4> Popular posts</h4>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam autem assumend
					</p>
					<p>
						Vitae ullam ipsam rem ratione sit facere ut. Natus voluptates fugiat quaerat
					</p>
					<p>
						Sapiente perferendis quis consequatur exercitationem sed facilis.
					</p>
				</div>

				<div class="col">
					<h4>About us</h4>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. A cupiditate, doloremque recusandae pariatur tempora placeat? Commodi, non
					</p>
					<p>
						Fugit dolore delectus placeat veritatis autem consequuntur sit consequatur sint dolorum? Voluptates, ipsum.
					</p>
				</div> -->


			</div>
			<!-- ./container -->

			<hr />
			<div class="footer-copy-line">
				<p class="copyrights">
					&copy; 2016 Marble. All rights reserved. Theme by elemis.
				</p>
				<!-- ./copyrights -->

				<?php 
				//to display a navigation menu
				//calls for an array of arguments
				// https://developer.wordpress.org/reference/functions/wp_nav_menu/
				wp_nav_menu( array(
					'theme_location' => 'footer'
				)); ?>
				
			</div>
			
		</div>
	</footer>
	<!-- ./main-footer -->
	<?php wp_footer(); ?>
</body>
</html>