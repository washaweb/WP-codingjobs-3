<?php get_header(); ?>
<section class="main-section">
	<div class="wrapper">
		<?php
			//the loop arguments come from wp() query
			if( have_posts() ): //if there is any result to display
				//loop throught the result
				while ( have_posts() ) : 
					the_post(); //set the post information here
					//you can display your post code here
					//this is executed for each post to display on a page
					//the_permalink()-> echoes the link to a post single view
					//the_title()-> echoes post title
					//the_content()-> echoes post content (html format)
					//the_excerpt()-> echoes post excerpt (text format)
					//the_date() -> echoes the post publication date
					//the_author(), the_category(', '), the_terms(), the_ID().... a lot of other tags to display post informations
					?>
		

			<div class="entry-content">
				<?php 
					//display a post thumbnail on the list pages only if post has a featured image
					if( has_post_thumbnail() && !is_singular() ): 
				?>
					<div class="featured-image">
						<!-- find the template tag to display a post thumbnail (look at the codex !!!) -->
						<?php the_post_thumbnail('home-thumb'); ?>
					</div>
				<?php endif; ?>
				<div class="post-content">
					<?php
						if( !is_singular() ):
							echo '<h1><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h1>';
						else: 
							echo '<h1>'.get_the_title().'</h1>';
						endif;

						//is_singular() -> conditional tag to test if the template renders a singular post entry
						//it can be a page or a post
						//is_single() -> for a singular post
						//is_page() -> for a singular page
						if( is_singular() ):
							//on a detail (singular) page or post, we display the full content
							the_content(); //displays the full content of a post
						else:
							//on an archive or on the blog home page, we display the excerpt
							the_excerpt(); //displays only the resume of a post
						endif;
					?>
				</div>
			</div>
		
		<!-- ./entry-post -->
		<?php
				endwhile;
			endif;
		?>
	</div>
</section>
<?php get_footer(); ?>